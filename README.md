React Redux SPA Boilerplate
=======================

Originally cloned from [React-Redux-Starter-Kit](https://github.com/davezuko/react-redux-starter-kit)

This repo aims to be a starter point for a multi-app single page application using React, Redux and React-Router.

Run in development mode:

```shell
$ npm run dev:no-debug
```

Compile into production

```shell
$ npm run deploy
```

Refer to the original repo for all available scripts.

Use TAB as line identation!

If the port 3000 is already taken, an error will be raised. Fix that by running:

```shell
$ PORT=*NEWPORT* npm run dev:no-debug
```

The following command runs tests once and generates coverage.

```shell
$ npm run test
```

The following command runs tests and watches them.

```shell
$ npm run test:dev
```