import React, { PropTypes } from 'react';

export default class ContainerApp extends React.Component {
	render () {
		return (
			// Put all app's global styles here
			<div>{this.props.children}</div>
		);
	}
}

ContainerApp.propTypes = {
	children: PropTypes.object
};