import React, { PropTypes } from 'react';

export default class WorkflowApp extends React.Component {
	render () {
		return (
			// Put all specific workflow styles here
			<div className='main-fs-app'>{this.props.children}</div>
		);
	}
}

WorkflowApp.propTypes = {
	children: PropTypes.object
};