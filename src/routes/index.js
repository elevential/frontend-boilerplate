import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

// NOTE: here we're making use of the `resolve.root` configuration
// option in webpack, which allows us to specify import paths as if
// they were from the root of the ~/src directory. This makes it
// very easy to navigate to files regardless of how deeply nested
// your current file is.
import ContainerApp from 'containers/ContainerApp';
import AllApps from 'components/AllApps';

import WorkflowApp from 'containers/WorkflowApp';
import NoWorkflow from 'components/NoWorkflow';
import CreateWorkflow from 'components/CreateWorkflow';

import SampleApp from 'containers/SampleApp';
import SampleComponent from 'components/SampleComponent';

import NotFound from 'components/NotFound';

export default (store) => (
	<Route path='/' component={ContainerApp}>
		<IndexRoute component={AllApps} />

		{/* Config routes for all apps */}
		<Route path='/workflow/' component={WorkflowApp}>
			<IndexRoute component={NoWorkflow} />
			<Route path='create' component={CreateWorkflow} />
		</Route>
		<Route path='/sample/' component={SampleApp}>
			<IndexRoute component={SampleComponent} />
		</Route>

		{/* Redirect base URL for all apps */}
		<Redirect from="workflow" to="/workflow/" />
		<Redirect from="sample" to="/sample/" />

		{/* URL don't match any of above, render not found component */}
		<Route path='*' component={NotFound} />
	</Route>
);