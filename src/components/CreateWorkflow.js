import React from 'react';

const CreateWorkflow = (props) => {
	return (
		<form>
			<div className='form-group'>
				<h3><label>Nome</label></h3>
				<input type='text' className='form-control input-lg' style={{width: "400px"}} />
			</div>
			<div className='form-group'>
				<h3><label>Tipo</label></h3>
				<select className='form-control input-lg'>
					<option>Coleta</option>
					<option>Processamento</option>
					<option>Híbrido</option>
				</select>
			</div>
			<button type='submit' className='btn btn-default btn-block'>Criar</button>
		</form>
	);
};

export default CreateWorkflow;