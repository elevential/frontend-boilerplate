import React from 'react';
import { Link } from 'react-router';

const NoWorkflow = (props) => {
	return (
		<div className="text-center">
			<h2>Hum, parece que você ainda não tem nenhum workflow...</h2>
			<br />
			<Link to="/workflow/create" className="btn btn-default btn-lg">
				<i className="fa fa-plus"></i> Criar
			</Link>
			<br />
			<h2>ou</h2>
			<br />
			<button 
				type="button" 
				className="btn btn-default btn-lg" >
				<i className="fa fa-clone"></i> Clonar
			</button>           
		</div>
	);
};

export default NoWorkflow;