import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { increment, doubleAsync } from '../redux/modules/counter';
import classes from '../styles/SampleComponent.scss';

// We avoid using the `@connect` decorator on the class definition so
// that we can export the undecorated component for testing.
// See: http://rackt.github.io/redux/docs/recipes/WritingTests.html
export class SampleComponent extends React.Component {
	static propTypes = {
		counter: PropTypes.number.isRequired,
		doubleAsync: PropTypes.func.isRequired,
		increment: PropTypes.func.isRequired
	};

	render () {
		return (
			<div className='container text-center'>
				<h1>Welcome to the React Redux Starter Kit :)</h1>
				<h2>
					Sample Counter:
					{' '}
					<span className={classes['counter--green']}>{this.props.counter}</span>
				</h2>
				<button className='btn btn-default' onClick={this.props.increment}>
					Increment
				</button>
				{' '}
				<button className='btn btn-default' onClick={this.props.doubleAsync}>
					Double (Async)
				</button>
			</div>
		);
	}
}

const mapStateToProps = (state) => ({
	counter: state.counter
});
export default connect((mapStateToProps), {
	increment: () => increment(1),
	doubleAsync
})(SampleComponent);