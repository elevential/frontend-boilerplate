import React from 'react';
import { Link } from 'react-router';

const AllApps = (props) => {
	return (
		<div>
			<br />
			<ul>
				<li><Link to="/workflow">Workflow</Link></li>
				<li><Link to="/sample">Sample</Link></li>
			</ul>     
		</div>
	);
};

export default AllApps;